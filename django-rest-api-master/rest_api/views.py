from rest_framework import generics

from .serializers import TarefaSerializer
from .models import Tarefa
from django.contrib.auth.models import User


class CreateView(generics.ListCreateAPIView):
    queryset = Tarefa.objects.all()
    serializer_class = TarefaSerializer

class DetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Tarefa.objects.all()
    serializer_class = TarefaSerializer

class UserView(generics.ListAPIView):
    queryset = User.objects.all()

class UserDetailsView(generics.RetrieveAPIView):
    queryset = User.objects.all()

