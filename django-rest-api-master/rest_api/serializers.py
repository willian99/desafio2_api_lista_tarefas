from rest_framework import serializers
from .models import Tarefa
from django.contrib.auth.models import User


class TarefaSerializer(serializers.ModelSerializer):


    class Meta:

        model = Tarefa
        fields = ('id', 'task', 'done')




