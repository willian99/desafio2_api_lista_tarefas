from __future__ import unicode_literals

from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User


class Tarefa(models.Model):
    """This class represents the bucketlist model."""
    task = models.CharField(max_length=255, blank=False, unique=True)
    done = models.BooleanField(default=False)

    def __str__(self):

        return "{}".format(self.name)


